================
fault tree icons
================

An icon set for `fault tree analyses (FTA)
<https://en.wikipedia.org/wiki/Fault_tree_analysis>`__.

Icons are available in archives with the following formats:

- `SVG <https://gitlab.com/lpirl/fault-tree-icons/-/jobs/artifacts/main/raw/export/fault-tree-icons-svgs.tar.gz?job=archives>`__
- `PDF <https://gitlab.com/lpirl/fault-tree-icons/-/jobs/artifacts/main/raw/export/fault-tree-icons-pdfs.tar.gz?job=archives>`__
- `PNG <https://gitlab.com/lpirl/fault-tree-icons/-/jobs/artifacts/main/raw/export/fault-tree-icons-pngs.tar.gz?job=archives>`__
- `draw.io <https://draw.io>`__ `shape library <https://gitlab.com/lpirl/fault-tree-icons/-/jobs/artifacts/main/raw/export/drawio-library.xml?job=xml>`__

  - `start a new diagram with the icon set loaded <https://app.diagrams.net/?splash=0&clibs=Uhttps%3A%2F%2Fgitlab.com%2Flpirl%2Ffault-tree-icons%2F-%2Fjobs%2Fartifacts%2Fmain%2Fraw%2Fexport%2Fdrawio-library.xml%3Fjob%3Dxml>`__
  - to import in : File → Open Library From → URL::

      https://gitlab.com/lpirl/fault-tree-icons/-/jobs/artifacts/main/raw/export/drawio-library.xml?job=xml

Single icon files can be download by `browsing the latest CI artifacts
<https://gitlab.com/lpirl/fault-tree-icons/-/jobs/artifacts/main/browse/export/?job=images>`__.

license
=======

Since the icons where forked from `The Open Reliability Editor (ORE,
formerly FuzzEd) <https://github.com/troeger/fuzzed/>`__, the icons licensed `AGPL v3 <https://opensource.org/license/agpl-v3/>`__.
