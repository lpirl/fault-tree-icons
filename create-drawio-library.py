#!/usr/bin/env python3
"""
This script converts mxGraph shape files to a mxlibrary,
e.g., for draw.io/diagrams.net.

Thanks IPyDrawio for tutorial on creating custom shape libraries:
https://ipydrawio.readthedocs.io/en/stable/tutorials/working-with-custom-libraries/index.html
"""

import argparse
from base64 import b64encode
from xml.etree import ElementTree
from json import dumps as dumps_json
from zlib import compressobj
from urllib.parse import quote
from xml.sax.saxutils import escape


GRAPHMODEL_TEMPLATE = """
<mxGraphModel>
  <root>
    <mxCell id="0"/>
    <mxCell id="1" parent="0"/>
    <mxCell id="2" value="" vertex="1" parent="1"
      style="shape=stencil({stencil});whiteSpace=wrap;html=1;">
      <mxGeometry width="{width}" height="{height}" as="geometry"/>
    </mxCell>
  </root>
</mxGraphModel>
"""


CONNECTIONS = ElementTree.fromstring("""
<connections>
  <constraint x="0.5" y="0.0" perimeter="0" name="N"/>
  <constraint x="0.5" y="1.0" perimeter="0" name="S"/>
  <constraint x="0.0" y="0.5" perimeter="0" name="W"/>
  <constraint x="1.0" y="0.5" perimeter="0" name="E"/>
</connections>
""")


def pack(string):
  ''' packs XML according to draw.io base64/zlip method '''
  defalte = compressobj(wbits=-15)
  return b64encode(
    defalte.compress(
      quote(string).encode("utf-8")) + defalte.flush(),
  ).decode("utf-8")


def main():
  '''
  Actual main procedure.

  Uncaught exceptions will be handled in calling procedure.
  '''

  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
  )

  parser.add_argument('in_files', nargs='+',
                      help='mxGraph shape files to assemble as mxlibrary')
  parser.add_argument('out_file', help='file to write mxlibrary to')

  args = parser.parse_args()

  lib = []

  for in_file in args.in_files:
    for shape in ElementTree.parse(in_file).getroot():

      if shape.tag != "shape":
        print(f"unknown element {shape.tag} in {in_file}")
        continue

      for connections in shape.findall('connections'):
        shape.remove(connections)
      shape.append(CONNECTIONS)

      width = float(shape.attrib['w'])
      height = float(shape.attrib['h'])


      graphmodel = GRAPHMODEL_TEMPLATE.format(
        stencil=pack(ElementTree.tostring(shape)),
        width=width,
        height=height,
      ).strip()

      lib.append({
        'name': shape.attrib['name'],
        'w': width,
        'h': height,
        'xml': escape(graphmodel),
        'aspect': 'fixed',
      })

  json = dumps_json(lib, indent="  ")

  xml = f"<mxlibrary>{json}</mxlibrary>"

  with open(args.out_file, "w") as out_fp:
    out_fp.write(xml)
    out_fp.write('\n')


if __name__ == '__main__':
  main()
