MAKEFLAGS += -r

IN_DIR ?= drawings
OUT_DIR ?= export

IN_SVGS ?= $(wildcard $(IN_DIR)/*.inkscape.svg)
_OUT_BASEPATHS ?= $(patsubst $(IN_DIR)/%,$(OUT_DIR)/%,\
	$(IN_SVGS:.inkscape.svg=))

COMMON_EXPORT_OPTS = \
	--vacuum-defs \
	--export-overwrite \
	--export-area-page \
	--export-text-to-path \
	--export-filename $@ \
	$<

# heights for raster image exports:
#                          SVGA HD  f/HD 4k   8k
HEIGHTS = 16 32 64 128 512 600  720 1080 2160 4320

ARCHIVE_CMD ?= \
	tar c --transform "s/^$(OUT_DIR)//" $^ \
	| gzip --best > $@

.SECONDEXPANSION:
images: $$(IMAGES)
archives: $$(ARCHIVES)

$(OUT_DIR):
	mkdir -p $@

# SVG
#####

SVGS ?= $(addsuffix .svg,$(_OUT_BASEPATHS))
IMAGES += $(SVGS)

svgs: $(SVGS)

$(OUT_DIR)/%.svg: $(IN_DIR)/%.inkscape.svg | $(OUT_DIR)
	inkscape $(COMMON_EXPORT_OPTS) --export-plain-svg

ARCHIVES += $(OUT_DIR)/fault-tree-icons-svgs.tar.gz

$(OUT_DIR)/fault-tree-icons-svgs.tar.gz: $(SVGS)
	$(ARCHIVE_CMD)

# pdf
#####

PDFS ?= $(addsuffix .pdf,$(_OUT_BASEPATHS))
IMAGES += $(PDFS)

pdfs: $(PDFS)

$(OUT_DIR)/%.pdf: $(IN_DIR)/%.inkscape.svg | $(OUT_DIR)
	inkscape $(COMMON_EXPORT_OPTS)

ARCHIVES += $(OUT_DIR)/fault-tree-icons-pdfs.tar.gz

$(OUT_DIR)/fault-tree-icons-pdfs.tar.gz: $(PDFS)
	$(ARCHIVE_CMD)

# png
#####

PNGS ?= $(foreach height,$(HEIGHTS),\
  $(addsuffix -$(height)p.png,$(_OUT_BASEPATHS)))
IMAGES += $(PNGS)

pngs: $(PNGS)

define PNG_template =
$(OUT_DIR)/%-$(1)p.png: $(IN_DIR)/%.inkscape.svg | $(OUT_DIR)
	inkscape $$(COMMON_EXPORT_OPTS) --export-type=png --export-height=$(1)
endef
$(foreach height,$(HEIGHTS),$(eval $(call PNG_template,$(height))))

ARCHIVES += $(OUT_DIR)/fault-tree-icons-pngs.tar.gz

$(OUT_DIR)/fault-tree-icons-pngs.tar.gz: $(PNGS)
	$(ARCHIVE_CMD)

# mxgraphs
##########

.PHONY: svg2shape
svg2shape:
	[ -d svg2shape ] \
	&& git -C svg2shape pull \
	|| git clone --depth 1 \
		https://github.com/process-analytics/mxgraph-svg2shape.git svg2shape
	cd svg2shape && ./mvnw package

MXGRAPHS ?= $(addsuffix .xml,$(_OUT_BASEPATHS))

mxgraphs: $(MXGRAPHS)

$(OUT_DIR)/%.xml: $(OUT_DIR)/%.svg | svg2shape
	cd svg2shape \
	&& java -jar target/mxgraph-svg2shape-*-jar-with-dependencies.jar \
			../$^ ../$(OUT_DIR)

drawio-library: $(OUT_DIR)/drawio-library.xml
$(OUT_DIR)/drawio-library.xml: $(MXGRAPHS)
	./create-drawio-library.py $^ $@

# clean
#######

clean:
	rm -f $(IMAGES) $(ARCHIVES) $(OUT_DIR)/drawio-library.xml
	rmdir -p $(OUT_DIR)
